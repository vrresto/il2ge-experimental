/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <core/context.h>
#include <core/scene.h>
#include <gl_wrapper.h>

namespace core
{


Context* Context::s_current = nullptr;


Context::Context()
{
  core_gl_wrapper::texture_state::freeze();
  m_scene = std::make_unique<core::Scene>();
  core_gl_wrapper::texture_state::restore();
}


Context::~Context() {}


} // namespace core

