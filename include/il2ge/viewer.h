/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_VIEWER_H
#define IL2GE_VIEWER_H

#include <render_util/viewer/viewer.h>

#include <INIReader.h>


namespace il2ge
{


class ViewerConfiguration : public render_util::viewer::Configuration
{
  INIReader m_ini;

public:
  ViewerConfiguration(std::string path) : m_ini(path)
  {
    if (m_ini.ParseError())
      throw std::runtime_error(path + ": parse error at line " + std::to_string(m_ini.ParseError()));
  }

  std::string getValue(std::string section, std::string key) override
  {
    return m_ini.Get(section, key, "");
  }
};


}


#endif
