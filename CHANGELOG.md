# Changelog

## [Unreleased]

### Fixed
- #44 - Crash with game version 4.15 (thanks to SAS_Storebror)
- Terrain outside of the map borders is always covered by water.  
  The new behaviour is to repeat the textures at the map border like IL-2 does.

## [0.2.0]

### Fixed
- #20 - Bombsight has wrong color
- #25 - Shadows on water have wrong color
- #14 - Lines are displayed on water and land

## [0.1.0]

- First release
