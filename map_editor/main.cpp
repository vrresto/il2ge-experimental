/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#define GLM_ENABLE_EXPERIMENTAL

#include <il2ge/exception_handler.h>
#include <il2ge/map_loader.h>
#include <il2ge/viewer.h>
#include <render_util/viewer/viewer.h>
#include <render_util/image_loader.h>
#include <render_util/physics.h>
#include <util.h>
#include <log.h>

#include <INIReader.h>

#include <glm/glm.hpp>
#include <glm/gtx/vec_swizzle.hpp>
#include <iostream>

#ifdef _WIN32
  #include <windows.h>
  #include <shlobj.h>
  #include <direct.h>
#else
  #include <portable-file-dialogs.h>
#endif

using namespace std;
using namespace glm;


bool il2ge::map_loader::isDumpEnabled()
{
  return false;
}


namespace
{


const std::string APP_NAME = "il2ge_map_editor";


class MapGeography
{
  glm::dvec2 m_origin {};
  double m_meters_per_degree_longitude {};
  std::string m_map_projection;
  bool m_valid = false;

public:
  MapGeography() {}

  MapGeography(glm::dvec2 origin, double meters_per_degree_longitude, string map_projection) :
    m_origin(origin), m_meters_per_degree_longitude(meters_per_degree_longitude),
    m_map_projection(map_projection)
  {
    assert(!m_map_projection.empty());
    assert(m_meters_per_degree_longitude > 0);
    m_valid = true;
  }

  MapGeography(INIReader &ini, string ini_path)
  {
    auto latitude = ini.GetReal("Geography", "MapOriginLatitude", -1000.0);
    auto longitude = ini.GetReal("Geography", "MapOriginLongitude", -1000.0);

    if (latitude < -90 || latitude > 90)
      throw runtime_error(ini_path + ": Latitude invalid: " + to_string(latitude));

    if (longitude < -180 || longitude > 180)
      throw runtime_error(ini_path + ": Longitude invalid: " + to_string(longitude));

    m_origin = glm::dvec2(longitude, latitude);

    m_meters_per_degree_longitude = ini.GetReal("Geography", "MetersPerDegreeLongitude", 0.0);

    if (m_meters_per_degree_longitude <= 0)
      throw runtime_error(ini_path +
        ": MetersPerDegreeLongitude invalid : " +
        to_string(m_meters_per_degree_longitude));

    m_map_projection = ini.Get("Geography", "MapProjection", "");
    if (m_map_projection.empty())
      throw runtime_error(ini_path + ": MapProjection not set");

    m_valid = true;
  }

  void setDefaults()
  {
    m_meters_per_degree_longitude =
      render_util::physics::EARTH_CIRCUMFERENCE / 360.0;
    m_map_projection = "mercator-spherical";
    m_origin = glm::dvec2(0);
    m_valid = true;
  }

  string getMapProjection()
  {
    assert(!m_map_projection.empty());
    return m_map_projection;
  }

  void save(ostream &out)
  {
    assert(m_valid);
    out.precision(std::numeric_limits<double>::digits10);
    out << "[Geography]" << endl;
    out << "MapOriginLongitude = " << m_origin.x << endl;
    out << "MapOriginLatitude = " << m_origin.y << endl;
    out << "MetersPerDegreeLongitude = " <<  m_meters_per_degree_longitude << endl;
    out << "MapProjection = " << m_map_projection << endl;
  }

  bool isValid() { return m_valid; }
  glm::dvec2 getOrigin() const { return m_origin; }
  double getMetersPerDegreeLongitude() const { return m_meters_per_degree_longitude; }
};


#ifdef _WIN32

std::string getExeFilePath()
{
  char module_file_name[MAX_PATH];

  if (!GetModuleFileNameA(0,
      module_file_name,
      sizeof(module_file_name)))
  {
    abort();
  }

  return module_file_name;
}


int showOkCancelDialog(std::string text, std::string title)
{
  return MessageBoxA(nullptr, text.c_str(), title.c_str(), MB_OKCANCEL | MB_ICONQUESTION)
      == IDOK;
}

#else

bool showOkCancelDialog(std::string text, std::string title)
{
  return pfd::message(title, text, pfd::choice::ok_cancel,
                      pfd::icon::question).result()
      == pfd::button::ok;
}

#endif


struct ElevationMapLoader : public render_util::viewer::ElevationMapLoaderBase
{
  std::string m_map_path;
  std::string m_load_ini_path;
  std::string m_load_il2ge_ini_path;
  std::string m_base_map_path;
  std::string m_base_map_data_path;
  std::string m_base_map_properties_path;
  std::string m_water_map_chunks_path;
  std::string m_water_map_table_path;
  std::string m_base_water_map_texture_path;
  bool m_has_base_map = false;
  glm::ivec2 m_base_map_size {};
  glm::ivec2 m_map_size_px {};

  MapGeography m_base_map_geography;
  MapGeography m_detail_map_geography;

  ElevationMapLoader(std::string map_path, std::string base_map_path)
  {

    auto map_dir = util::getDirFromPath(map_path);

    bool map_path_is_ini = util::makeLowercase(util::getFileExtensionFromPath(map_path)) == "ini";

    if (map_path_is_ini)
    {
      m_load_ini_path = map_path;
      m_load_il2ge_ini_path = map_dir + '/' + util::basename(map_path, true) + ".il2ge.ini";

      INIReader ini(map_path);
      if (!ini.ParseError())
      {
        auto heightmap = ini.Get("MAP", "HeightMap", "");
        assert(!heightmap.empty());
        m_map_path = map_dir + '/' + heightmap;

        auto map_c = ini.Get("MAP", "ColorMap", "");
        assert(!map_c.empty());
        m_water_map_chunks_path = map_dir + '/' + map_c;
        m_water_map_table_path = m_water_map_chunks_path + "_table";
      }
      else
      {
        throw std::runtime_error("Error parsing " + map_path
                                  + " at line " + std::to_string(ini.ParseError()));
      }
    }
    else
    {
      m_map_path = map_path;
    }

    assert(base_map_path.empty());
//     m_base_map_path = base_map_path;

    if (!m_load_il2ge_ini_path.empty())
    {
      if (util::fileExists(m_load_il2ge_ini_path))
      {
        INIReader ini(m_load_il2ge_ini_path);
        if (!ini.ParseError())
        {
//           if (m_base_map_path.empty())
//             m_base_map_path = ini.Get("Geography", "BaseMap", "");
          m_detail_map_geography = MapGeography(ini, m_load_il2ge_ini_path);
        }
        else
        {
          throw std::runtime_error("Error parsing " + m_load_il2ge_ini_path
                                    + " at line " + std::to_string(ini.ParseError()));
        }
      }
    }

    assert(!m_map_path.empty());

    if (m_base_map_path.empty())
    {
      m_base_map_path = "il2ge_base_map/" + util::basename(map_dir);
      m_base_map_data_path = m_base_map_path + ".data";
      m_base_map_properties_path = m_base_map_path + ".properties";
      m_base_water_map_texture_path = m_base_map_path + "_water.tga";
    }

    if (!m_base_map_path.empty())
    {
      if (util::fileExists(m_base_map_properties_path))
      {
        m_has_base_map = true;
        INIReader ini(m_base_map_properties_path);
        if (!ini.ParseError())
        {
          m_base_map_geography = MapGeography(ini, m_base_map_properties_path);
          m_base_map_size.x = ini.GetInteger("", "Width", 0);
          m_base_map_size.y = ini.GetInteger("", "Height", 0);
        }
        else
        {
          throw std::runtime_error("Error parsing " + m_base_map_properties_path
                                    + " at line " + std::to_string(ini.ParseError()));
        }

        if (!m_detail_map_geography.isValid())
        {
          m_detail_map_geography = m_base_map_geography;
        }
      }
    }

    if (!m_detail_map_geography.isValid())
    {
      m_detail_map_geography.setDefaults();
    }

    assert(!m_detail_map_geography.getMapProjection().empty());
  }

  glm::dvec2 getMapOrigin() const override
  {
    return m_detail_map_geography.getOrigin();
  }

  render_util::ElevationMap::Ptr createElevationMap() override
  {
    auto hm_image =
      render_util::loadImageFromFile<render_util::ImageGreyScale>(m_map_path);

    if (hm_image)
    {
      m_map_size_px = hm_image->getSize();
      return il2ge::map_loader::createElevationMap(hm_image);
    }
    else
      throw std::runtime_error("Failed to load height map: " + m_map_path);
  }

  int getMetersPerPixel() const override
  {
    return 200;
  }

  render_util::ElevationMap::Ptr createBaseElevationMapBin() const
  {
    LOG_INFO << "Opening " << m_base_map_data_path << std::endl;
    try
    {
      util::NormalFile file(m_base_map_data_path);
      return il2ge::map_loader::createBaseElevationMap(file, m_base_map_size);
    }
    catch (std::exception &e)
    {
      LOG_ERROR << e.what() << std::endl;
      return {};
    }
  }

  render_util::ElevationMap::Ptr createBaseElevationMapTGA() const
  {
      auto hm_image =
        render_util::loadImageFromFile<render_util::ImageGreyScale>(m_base_map_path);

      if (hm_image)
        return il2ge::map_loader::createElevationMap(hm_image);
      else
        exit(1);
  }

  render_util::ElevationMap::Ptr createBaseElevationMap() const override
  {
    if (!m_base_map_path.empty() && m_has_base_map)
    {
      auto ext = util::makeLowercase(util::getFileExtensionFromPath(m_base_map_path));

      if (ext == "tga")
      {
        return createBaseElevationMapTGA();
      }
      else if(ext.empty())
      {
        return createBaseElevationMapBin();
      }
      else
      {
        cerr<<"Unhandled extension: "<<ext<<endl;
        exit(1);
      }
    }
    else
      return {};
  }

  int getBaseElevationMapMetersPerPixel() const override
  {
    assert(m_has_base_map);
    return 200;
  }


  double getBaseMapMetersPerDegreeLongitude() const override
  {
    assert(m_has_base_map);
    assert(m_base_map_geography.getMetersPerDegreeLongitude() > 0);
    return m_base_map_geography.getMetersPerDegreeLongitude();
  }

  double getDetailMapMetersPerDegreeLongitude() const override
  {
    assert(m_detail_map_geography.getMetersPerDegreeLongitude() > 0);
    return m_detail_map_geography.getMetersPerDegreeLongitude();
  }

  glm::dvec2 getBaseElevationMapOrigin() const override
  {
    assert(m_has_base_map);
    return m_base_map_geography.getOrigin();
  }

  std::string getBaseMapPath() override
  {
    return m_base_map_path;
  }

  void saveMapAttributes(const glm::dvec2 &origin,
                         double meters_per_degree_longitude,
                         string map_projection) override
  {
    std::string ini_path;

    if (!m_load_il2ge_ini_path.empty())
      ini_path = m_load_il2ge_ini_path;
    else
      ini_path = m_map_path + ".ini";

    if (util::fileExists(ini_path))
    {
      if (!showOkCancelDialog("Overwrite " + ini_path + "?", "Confirm overwrite"))
        return;
    }

    std::ofstream out(ini_path);
    MapGeography g(origin, meters_per_degree_longitude, map_projection);
    g.save(out);
    out << "BaseMap = " << m_base_map_path << std::endl;
  }

  void reloadBaseMap() override
  {
    INIReader ini(m_base_map_properties_path);
    if (!ini.ParseError())
    {
      m_has_base_map = true;
      m_base_map_geography = MapGeography(ini, m_base_map_properties_path);
    }
    else
    {
      throw std::runtime_error("Error parsing " + m_base_map_properties_path
                                + " at line " + std::to_string(ini.ParseError()));
    }
  }

  std::string getMapProjection() override
  {
    return m_detail_map_geography.getMapProjection();
  }

  render_util::TerrainBase::WaterMap loadWaterMap() override
  {
    auto chunks = render_util::loadImageFromFile<render_util::ImageGreyScale>(
      m_water_map_chunks_path);
    assert(chunks);

    auto table = util::readFile<char>(m_water_map_table_path);
    assert(!table.empty());

    return il2ge::map_loader::createWaterMap(m_map_size_px, chunks, table);
  }

  render_util::ImageGreyScale::Ptr loadBaseWaterMap() override
  {
    return render_util::loadImageFromFile<render_util::ImageGreyScale>(
      m_base_water_map_texture_path);
  }

  std::string getBaseWaterMapPath() override
  {
    return m_base_water_map_texture_path;
  }
};


} // namespace


int main(int argc, char **argv)
{
#ifdef _WIN32
  il2ge::exception_handler::install(APP_NAME + "_crash.log");
  il2ge::exception_handler::watchModule(GetModuleHandle(0));
#endif

  string map_path;
  string base_map_path;

  if (argc == 2)
  {
    map_path = argv[1];
  }
  else if (argc == 3)
  {
    map_path = argv[1];
    base_map_path = argv[2];
  }
  else
  {
    cerr << "Wrong number of arguments: " << argc << endl;
    return 1;
  }

  assert(!map_path.empty());

  std::unique_ptr<il2ge::ViewerConfiguration> config;
  try
  {
    config = std::make_unique<il2ge::ViewerConfiguration>(APP_NAME + ".ini");
  }
  catch (std::exception &e)
  {
    std::cerr << e.what() << std::endl;
  }

  render_util::viewer::CreateElevationMapLoaderFunc create_map_loader_func = [map_path, base_map_path] ()
  {
    return make_shared<ElevationMapLoader>(map_path, base_map_path);
  };

  render_util::viewer::runMapEditor(create_map_loader_func, APP_NAME, config.get());
}
